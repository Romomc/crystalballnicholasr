package andriod.romon.crystalball;

public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions () {
        answers = new String[] {
                "Your wishes will come true",
                "Your wishes will NEVER come true",
                "Don't try it",
                "Go for it",
                "Not in your lifetime",

        };
    }

    public static Predictions get() {
        if(predictions == null) {
        predictions = new Predictions();
        }
        return predictions;
    }

    public String getPrediction() {
        return answers[1];
    }
}
